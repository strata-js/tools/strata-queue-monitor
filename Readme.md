# Strata Queue Monitor

The Strata Queue Monitor (SQM) is a [Strata.js][strata] service that writes all messages to an 
[Elasticsearch Cluster][es]. This allows you to monitor and log every message sent to and from a [Strata.js][strata] 
service. This is an incredibly powerful debugging tool, however, it should have a low impact on production traffic, so 
it is considered safe to run in production as well.

## How does it work?

SQM is a normal [Strata.js][strata] service; it processes messages on a queue, does some work on them, and then 
responds. (In the case of logging messages, the response is always empty.) It scales like other services, supports 
everything other services support, and is deployed like a normal service. There's nothing special or magical about 
SQM from that perspective.

Where it's trick comes in is this: the `monitor.logMesssages` operation takes a payload of `queue` and `envelope`. 
This allows SQM to take the passed in envelope and queue and log that out to a special elastic search index, 
allowing for monitoring of message traffic. If you connect up Kibana you have a very user-friendly log of all the 
messages in your system:

![Kibana](docs/images/kabana.png)

What's more, since SQM is build on redis streams, it has the same assurances from message acknowledgement. This 
allows for resilient message logging, and ensures that a crashed monitor service can be correctly resumed without 
any loss of messages.

You can, of course, configure SQM to put a maximum length on the queue, so that there _could_ be a loss of messages 
if SQM is down for long enough, or overloaded enough, but that's a decision that should be based on the work load an 
importance of capturing every log.

### How do I log to it?

Part of the elegance of SQM is that it's just a normal service, and it can be sent messages like any other service. 
Technically, it doesn't even care if what it's being told to log are even real messages, as long as they match the 
Strata Request or Response envelope structure.

This elegance is a two-edged sword, however. It's easy to log to this service in a one-off manner, but capturing 
every request and response involves a better architected solution. Because of that, SQM has a [companion middleware]
[mlmw] 
that will log the request and response of every processed message.

The [message-logging middleware][mlmw] takes the queue to log messages to (and optionally the context/operation, if 
you're not using SQM) and it logs all incoming requests and responses from the service to the specified queue. This 
logging happens without waiting for a response; any error encountered will be logged as a warning, but that is the 
only record of the failure.

_(See the Message Logging Middleware [docs][mlmw] for usage instructions.)_

[mlmw]: https://gitlab.com/strata-js/middleware/message-logging

## Usage

The recommended way of deploying SQM is with docker. We publish an official docker image, which can be used as follows:

```bash
$ docker run -d --name sqm -v <path/to/config>/config.yml:/app/config/config.yml stratajs/strata-queue-monitor:latest
```

You can also publish it as part of a `docker-compose.yml` file:

```yaml
version: '3'

services:
    sqm:
        image: stratajs/strata-queue-monitor:latest
        restart: unless-stopped
        volumes:
            - ../config:/app/config:ro
```

### Configuration

SQM has a well documented, and easy to modify `config.yml` file. We recommend you copy that file from either this 
repository or from the docker image (using the `docker cp` command) and use it as a starting point.

## Development

In order to develop SQM, you will need a local elastic search cluster. We recommend a single node cluster, with a 
kibana instance pointed to allow easy viewing of documents. It aid in this, we provide a docker-compose file to set 
up a development environment.

### Compose File

To set up the development environment, make sure you have [docker-compose][compose] installed. Then run:

```bash
$ docker-compose -p strata-dev -f compose/dev-env.yml up -d
```

Now, you can bring up kibana at [http://localhost:5601](http://localhost:5601/). You will need to set up an index 
pattern in order to view the SQM messages. In order to do so, go to "Stack Management" and then "Index Patterns". 
(You will want to have sent some messages by this point, in order to build the index.) Simply add an index patter of 
`strata-queue-monitor-*`.

Now, you can go to "Discover", select `strata-queue-monitor-*`, and see all the messages that are logged by SQM.

[es]: https://www.elastic.co/
[strata]: https://gitlab.com/strata-js/strata
[streams]: https://redis.io/topics/streams-intro
[compose]: https://docs.docker.com/compose/
