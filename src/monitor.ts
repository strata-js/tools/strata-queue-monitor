// ---------------------------------------------------------------------------------------------------------------------
// Strata Queue Monitor Service
// ---------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import { hostname } from 'node:os';
import { StrataService, logging } from '@strata-js/strata';
import configUtil from '@strata-js/util-config';

// Models
import { StrataQueueMonitorConfig } from './models/config.js';

// Contexts
import messagesContext from './context/monitor.js';

// Managers
import messageMan from './managers/messages.js';

// ---------------------------------------------------------------------------------------------------------------------

// Determine environment
// eslint-disable-next-line no-multi-assign
const env = process.env.ENVIRONMENT = process.env.ENVIRONMENT ?? 'local';
process.env.HOSTNAME = process.env.HOSTNAME ?? hostname();

// Load config
configUtil.load(`./config/config.yml`);

// Set Logging config
logging.setConfig(configUtil.get<StrataQueueMonitorConfig>()?.logging ?? {});

// Create the service instance
const service = new StrataService(configUtil.get());

// Register Contexts
service.registerContext('monitor', messagesContext);

// Initialize Managers
await messageMan.init(env);

// Start monitoring.
await service.start();

// ---------------------------------------------------------------------------------------------------------------------
