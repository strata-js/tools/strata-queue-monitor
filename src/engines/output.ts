import { OutputPlugin } from '../plugins/index.js';
import { logging } from '@strata-js/strata';
import { StrataQueueMonitorMessage } from '../models/requests.js';

const logger = logging.getLogger('messageEngine');

export class OutputEngine
{
    #plugins : OutputPlugin[];
    #initPlugins : OutputPlugin[] = [];

    constructor(plugins : OutputPlugin[])
    {
        this.#plugins = plugins;
    }

    async init() : Promise<void>
    {
        for(const plugin of this.#plugins)
        {
            try
            {
                if(plugin.init)
                {
                    // eslint-disable-next-line no-await-in-loop
                    await plugin.init();
                }
                this.#initPlugins.push(plugin);
                logger.info(`Successfully initialized ${ plugin.pluginName } plugin.`);
            }
            catch (err)
            {
                if(err instanceof Error)
                {
                    logger.warn(`Failed to init() ${ plugin.pluginName }.`);
                    logger.error(err.toString());
                }
                else
                {
                    logger.warn(`Failed to init() ${ plugin.pluginName }.`);
                    logger.error(`${ err }`);
                }
            }
        }

        if(this.#initPlugins.length === 0)
        {
            throw new Error(`No plugins where successfully able to init().`);
        }
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        await Promise.all(this.#initPlugins.map((plugin) => plugin.writeMessage(msg)));
    }
}
