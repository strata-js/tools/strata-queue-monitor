// ---------------------------------------------------------------------------------------------------------------------
// Message Processing Engine
// ---------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/strata';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('messageEngine');

export type SupportedDecodeMethod = 'jwt';

// ---------------------------------------------------------------------------------------------------------------------

function _decodeJWT(jwt : string) : Record<string, unknown>
{
    const splitToken = jwt.split('.');
    if(splitToken.length < 2)
    {
        throw new Error(`Invalid JWS Token`);
    }

    try
    {
        const jsonStringHeaders = Buffer.from(splitToken[0], 'base64').toString();
        const jsonStringPayload = Buffer.from(splitToken[1], 'base64').toString();
        return {
            headers: JSON.parse(jsonStringHeaders),
            payload: JSON.parse(jsonStringPayload),
        };
    }
    catch (err)
    {
        throw new Error(`Invalid JWS Token`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export function decodeAuth(auth : string, method ?: SupportedDecodeMethod) : Record<string, unknown> | undefined
{
    try
    {
        if(method === 'jwt')
        {
            return _decodeJWT(auth);
        }
    }
    catch (error)
    {
        if(error instanceof Error)
        {
            logger.warn('Failed to decode auth:', error.stack);
        }
        else
        {
            logger.warn('Failed to decode auth:', error);
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
