// ---------------------------------------------------------------------------------------------------------------------
// Outputs.ts
// ---------------------------------------------------------------------------------------------------------------------

import { ClientOptions } from '@elastic/elasticsearch';
import { ElasticBulkOutputConfig } from '../plugins/index.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface BaseOutputConfig
{
    environment : string;
    enabled : boolean;
}

export type NoneOutputConfig = BaseOutputConfig;

export type ConsoleOutputConfig = BaseOutputConfig;

export interface LogstashHTTPOutputConfig extends BaseOutputConfig
{
    url : string;
}

export interface ElasticOutputConfig extends BaseOutputConfig
{
    indexPrefix ?: string;
    forceRefresh ?: boolean;
    bulkConfig : ElasticBulkOutputConfig;
    clientOptions : ClientOptions;
}

// ---------------------------------------------------------------------------------------------------------------------
