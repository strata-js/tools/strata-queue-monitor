// ---------------------------------------------------------------------------------------------------------------------
// Request Payloads
// ---------------------------------------------------------------------------------------------------------------------

import { XOR } from 'ts-essentials';
import { RequestEnvelope, ResponseEnvelope, ResponseMessage } from '@strata-js/strata';

// ---------------------------------------------------------------------------------------------------------------------

export interface LogMessageRequest
{
    queue : string,
    envelope : XOR<RequestEnvelope, ResponseEnvelope>
}

export interface StrataQueueMonitorMessage
{
    '@timestamp' : string;
    'id' : string;
    'context' : string;
    'operation' : string;
    'responseQueue' ?: string;
    'monitorRequest' ?: string;
    'monitorResponse' ?: string;
    'monitorRequestQueue' ?: string;
    'monitorResponseQueue' ?: string;
    'requestTimestamp' ?: string;
    'responseTimestamp' ?: string;
    'metadata' ?: Record<string, unknown>;

    // Kibana doesn't support 'flattened'.
    // @see: https://github.com/elastic/kibana/issues/25820

    // requestEnvelope ?: Record<string, unknown>;
    // responseEnvelope ?: Record<string, unknown>;
    // request ?: Record<string, unknown>;
    // response ?: Record<string, unknown>;

    'requestEnvelope' ?: string;
    'responseEnvelope' ?: string;
    'request' ?: string;
    'response' ?: string;
    'auth' ?: string;
    'status' ?: 'succeeded' | 'failed' | 'pending';
    'messages' ?: ResponseMessage[];
    'priorRequest' ?: string;
    'requestChain' ?: string[];
    'client' ?: string;
    'service' ?: string;
    'timeout' ?: number;
}

// ---------------------------------------------------------------------------------------------------------------------
