// ---------------------------------------------------------------------------------------------------------------------
// Configurations
// ---------------------------------------------------------------------------------------------------------------------
import { StrataConfig } from '@strata-js/strata';
import { ConsoleOutputConfig, ElasticOutputConfig, LogstashHTTPOutputConfig, NoneOutputConfig } from './outputs.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface AuthConfig
{
    decode ?: 'jwt';
    key ?: string;
    scrub ?: boolean;
}

export interface ProcessingConfig
{
    auth ?: AuthConfig;
}

export interface OutputConfigs
{
    elastic : ElasticOutputConfig;
    logstashHTTP : LogstashHTTPOutputConfig;
    console : ConsoleOutputConfig;
    none : NoneOutputConfig;
}

export interface StrataQueueMonitorConfig extends StrataConfig
{
    processing ?: ProcessingConfig;
    outputConfigs : OutputConfigs;
}

// ---------------------------------------------------------------------------------------------------------------------
