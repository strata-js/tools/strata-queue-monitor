// ---------------------------------------------------------------------------------------------------------------------
// MessageManager
// ---------------------------------------------------------------------------------------------------------------------

import { XOR } from 'ts-essentials';
import { RequestEnvelope, ResponseEnvelope } from '@strata-js/strata';
import configUtil from '@strata-js/util-config';

// Models
import { StrataQueueMonitorConfig } from '../models/config.js';
import { StrataQueueMonitorMessage } from '../models/requests.js';

// Engines
import { decodeAuth } from '../engines/message.js';
import { OutputEngine } from '../engines/output.js';

// Plugins
import { OutputPlugin, plugins } from '../plugins/index.js';

// ---------------------------------------------------------------------------------------------------------------------

class MessageManager
{
    #config ?: StrataQueueMonitorConfig;
    #outputEngine ?: OutputEngine;

    async init(environment : string) : Promise<void>
    {
        this.#config = configUtil.get<StrataQueueMonitorConfig>();

        const outputConfigs = this.#config?.outputConfigs;
        if(!outputConfigs)
        {
            throw new Error('No outputConfig configuration key found.');
        }

        const outputInstances : OutputPlugin[] = [];
        for(const outputPluginName of Object.keys(outputConfigs))
        {
            const _config = outputConfigs[outputPluginName];
            _config['environment'] = environment;
            const _baseClass = plugins[outputPluginName];
            if(_baseClass && _config.enabled)
            {
                outputInstances.push(new _baseClass(_config));
            }
        }
        if(outputInstances.length === 0)
        {
            throw new Error('No output plugins are enabled,');
        }
        this.#outputEngine = new OutputEngine(outputInstances);
        await this.#outputEngine.init();
    }

    async handleMessage(queue : string, envelope : XOR<RequestEnvelope, ResponseEnvelope>) : Promise<void>
    {
        const currentTime = (new Date()).toISOString();
        let message : StrataQueueMonitorMessage;
        if(envelope.responseQueue)
        {
            const { timestamp, payload, auth, ...restRequest } = envelope;

            // We're working with a request
            message = {
                ...restRequest,
                '@timestamp': currentTime,
                'monitorRequestQueue': queue,
                'monitorRequest': currentTime,
                'requestTimestamp': timestamp,
                'request': JSON.stringify(payload),
                'requestEnvelope': JSON.stringify({ ...restRequest }),
            };

            // Handle processing of auth.
            if(auth && this.#config && this.#config.processing?.auth)
            {
                const decodedKey = this.#config.processing.auth.key ?? 'authDecoded';
                message[decodedKey] = decodeAuth(auth, this.#config.processing.auth.decode);
                if(this.#config.processing.auth.scrub ?? false)
                {
                    message.auth = 'SCRUBBED';
                }
                else
                {
                    message.auth = auth;
                }
            }
        }
        else
        {
            const { timestamp, payload, ...restResponse } = envelope;

            // We're working with a response
            message = {
                ...restResponse,
                '@timestamp': currentTime,
                'monitorResponse': (new Date()).toISOString(),
                'monitorResponseQueue': queue,
                'responseTimestamp': timestamp,
                'response': JSON.stringify(payload),
                'responseEnvelope': JSON.stringify({ ...restResponse }),
            };
        }
        if(!this.#outputEngine)
        {
            throw new Error(`Attempted to write message without first calling init() on OutputEngine.`);
        }
        await this.#outputEngine.writeMessage(message);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new MessageManager();

// ---------------------------------------------------------------------------------------------------------------------
