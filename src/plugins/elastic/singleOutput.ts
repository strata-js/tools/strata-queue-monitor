import { Client, ClientOptions } from '@elastic/elasticsearch';
import { OutputPlugin } from '../interfaces/outputPlugin.js';
import { createClient } from './httpClient.js';
import { StrataQueueMonitorMessage } from '../../models/requests.js';

export interface SingleOutputConfig
{
    indexPrefix : string;
    environment : string;
    forceRefresh ?: boolean;
    clientOptions : ClientOptions;
}

export class SingleOutputPlugin implements OutputPlugin
{
    #client ?: Client;
    #config : SingleOutputConfig;
    pluginName = 'Elastic (Single)';

    constructor(config : SingleOutputConfig)
    {
        this.#config = config;
    }

    #getDateStr() : string
    {
        const currDate = new Date();
        const offset = currDate.getTimezoneOffset();
        const newDate = new Date(currDate.getTime() - (offset * 60 * 1000));
        return newDate.toISOString().split('T')[0];
    }

    async init() : Promise<void>
    {
        this.#client = await createClient(this.#config.clientOptions, this.#config.indexPrefix);
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        if(!this.#client)
        {
            throw new Error(`${ this.pluginName } : Tried to call writeMessage() before calling init().`);
        }

        await this.#client.update({
            id: msg.id,
            index: `${ this.#config.indexPrefix }-${ this.#getDateStr() }`,
            refresh: this.#config.forceRefresh ?? false,
            // eslint-disable-next-line camelcase
            retry_on_conflict: 1,
            body: {
                doc: msg,
                // eslint-disable-next-line camelcase
                doc_as_upsert: true,
            },
        });
    }
}
