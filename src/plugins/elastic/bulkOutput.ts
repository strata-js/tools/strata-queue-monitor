// ---------------------------------------------------------------------------------------------------------------------
// Elastic Bulk Update
// ---------------------------------------------------------------------------------------------------------------------

import { Client, ClientOptions } from '@elastic/elasticsearch';
import { OutputPlugin } from '../interfaces/outputPlugin.js';
import { createClient } from './httpClient.js';
import { StrataQueueMonitorMessage } from '../../models/requests.js';
import { BulkStats, OnDropDocument } from '@elastic/elasticsearch/lib/Helpers';
import { logging } from '@strata-js/strata';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('httpClient');

// ---------------------------------------------------------------------------------------------------------------------

export interface BulkOutputConfig
{
    indexPrefix : string;
    environment : string;
    forceRefresh ?: boolean;
    flushSize : number;
    flushInterval : number
    concurrency : number;
    clientOptions : ClientOptions;
}

export class BulkOutputPlugin implements OutputPlugin
{
    #client ?: Client;
    #config : BulkOutputConfig;
    pluginName = 'Elastic (Bulk)';

    constructor(config : {
        clientOptions : ClientOptions;
        environment : string;
        maxMessages : number;
        flushInterval : number;
        indexPrefix : string;
        forceRefresh : boolean | undefined;
        enabled : boolean;
        flushSize : number;
        concurrency : number
    })
    {
        this.#config = config;
        // Deep copy the config and scrub the password before logging
        const scrubbedConfig = JSON.parse(JSON.stringify(this.#config));
        if(scrubbedConfig.clientOptions?.auth?.password)
        {
            scrubbedConfig.clientOptions.auth.password = '********';
        }
        logger.debug('Initializing Elastic (Bulk) with config:', JSON.stringify(scrubbedConfig, null, 2));
    }

    #getDateStr() : string
    {
        const currDate = new Date();
        const offset = currDate.getTimezoneOffset();
        const newDate = new Date(currDate.getTime() - (offset * 60 * 1000));
        return newDate.toISOString().split('T')[0];
    }

    #handleOnDocument(doc : StrataQueueMonitorMessage) : any
    {
        return [
            {
                update:
                    {
                        _index: `${ this.#config.indexPrefix }-${ this.#getDateStr() }`,
                        _id: doc.id,
                    },
            },
            {
                // eslint-disable-next-line camelcase
                doc_as_upsert: true,
            },
        ];
    }

    // TODO: Maybe handle errors better?
    #handleOnDrop(dropData : OnDropDocument<StrataQueueMonitorMessage>) : void
    {
        logger.warn(`Bulk Update failed (${ dropData.status }) : ${ dropData.error }`);
        const msg = (dropData.document as any).doc as StrataQueueMonitorMessage;
        logger.warn(`Failed MessageID: ${ msg.id }`);
    }

    #bulkUpdate(items : StrataQueueMonitorMessage[]) : Promise<BulkStats>
    {
        return new Promise((resolve, reject) =>
        {
            if(!this.#client)
            {
                throw new Error(`${ this.pluginName } : Tried to call #bulkUpdate() before calling init().`);
            }

            // It's a "thenable", but not actually a promise....
            this.#client.helpers.bulk({
                datasource: items,
                flushInterval: this.#config.flushInterval,
                flushBytes: this.#config.flushSize,
                onDocument: this.#handleOnDocument.bind(this),
                onDrop: this.#handleOnDrop.bind(this),
                retries: 1,
                concurrency: this.#config.concurrency,
                refreshOnCompletion: this.#config.forceRefresh ?? false,
            })
                .then((response) =>
                {
                    logger.debug('Successfully sent bulk update.');
                    resolve(response);
                })
                .catch((err) =>
                {
                    logger.error('Failed to send bulk update:', err.stack);
                    reject(err);
                });
        });
    }

    async init() : Promise<void>
    {
        this.#client = await createClient(this.#config.clientOptions, this.#config.indexPrefix);
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        if(!this.#client)
        {
            throw new Error(`${ this.pluginName } : Tried to call writeMessage() before calling init().`);
        }

        await this.#bulkUpdate([ msg ]);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
