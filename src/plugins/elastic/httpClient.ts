// ---------------------------------------------------------------------------------------------------------------------
// Strata Queue Monitor Service
// ---------------------------------------------------------------------------------------------------------------------

import { ApiResponse, Client, ClientOptions } from '@elastic/elasticsearch';
import { logging } from '@strata-js/strata';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('httpClient');

// ---------------------------------------------------------------------------------------------------------------------

const mappings = {
    properties: {
        '@timestamp': { type: 'date' },
        'id': { type: 'keyword' },
        'context': { type: 'keyword' },
        'operation': { type: 'keyword' },
        'responseQueue': { type: 'keyword' },
        'monitorRequest': { type: 'date' },
        'monitorResponse': { type: 'date' },
        'monitorRequestQueue': { type: 'keyword' },
        'monitorResponseQueue': { type: 'keyword' },
        'requestTimestamp': { type: 'date' },
        'responseTimestamp': { type: 'date' },

        // Kibana doesn't support 'flattened'.
        // @see: https://github.com/elastic/kibana/issues/25820

        // 'metadata': { type: 'flattened' },
        // 'requestEnvelope': { type: 'flattened' },
        // 'responseEnvelope': { type: 'flattened' },
        // 'request': { type: 'flattened' },
        // 'response': { type: 'flattened' },

        'metadata': { type: 'object' },
        'requestEnvelope': { type: 'text' },
        'responseEnvelope': { type: 'text' },
        'request': { type: 'text' },
        'response': { type: 'text' },
        'auth': { type: 'keyword' },
        'status': { type: 'keyword' },
        'messages': { type: 'nested' },
        'priorRequest': { type: 'keyword' },
        'requestChain': { type: 'keyword' },
        'client': { type: 'keyword' },
        'service': { type: 'keyword' },
        'timeout': { type: 'integer' },
        'authDecoded': { type: 'object' },
    },
};

export async function ensureIndex(indexPrefix : string, client : Client) : Promise<ApiResponse>
{
    return client.indices.putIndexTemplate({
        name: `${ indexPrefix }-message-index`,
        create: false,
        cause: 'Automatic upgrade of index template.',
        body: {
            // eslint-disable-next-line camelcase
            index_patterns: [ `${ indexPrefix }-*` ],
            priority: 9001,
            template: {
                mappings,
            },
        },
    });
}

export async function createClient(config : ClientOptions, indexPrefix : string) : Promise<Client>
{
    const client = new Client(config);
    try
    {
        await ensureIndex(indexPrefix, client);
        logger.info('Successfully ensured index template.');
    }
    catch (err)
    {
        if(err instanceof Error)
        {
            logger.error('Failed to ensure index template:', err.stack);
            throw err;
        }
        else
        {
            logger.error('Failed to ensure index template:', err);
            throw new Error('Failed to ensure index template.');
        }
    }

    return client;
}

// ---------------------------------------------------------------------------------------------------------------------
