import { ClientOptions } from '@elastic/elasticsearch';
import { OutputPlugin } from '../interfaces/outputPlugin.js';
import { SingleOutputPlugin } from './singleOutput.js';
import { BulkOutputPlugin } from './bulkOutput.js';
import { StrataQueueMonitorMessage } from '../../models/requests.js';

export interface ElasticBulkOutputConfig
{
    enabled : boolean;
    maxMessages : number;
    flushSize : number;
    flushInterval : number;
    concurrency : number;
}

export interface ElasticOutputConfig
{
    indexPrefix ?: string;
    environment : string;
    forceRefresh ?: boolean;
    bulkConfig : ElasticBulkOutputConfig;
    clientOptions : ClientOptions;
}

export class ElasticOutput implements OutputPlugin
{
    #config : ElasticOutputConfig;
    pluginName = 'Elastic';
    #pluginInstance : SingleOutputPlugin | BulkOutputPlugin;

    constructor(config : ElasticOutputConfig)
    {
        this.#config = config;

        if(this.#config.bulkConfig.enabled)
        {
            this.#pluginInstance = new BulkOutputPlugin({
                environment: this.#config.environment,
                forceRefresh: this.#config.forceRefresh,
                clientOptions: this.#config.clientOptions,
                indexPrefix: this.#config.indexPrefix ?? 'strata-queue-monitor',
                ...this.#config.bulkConfig,
            });
        }
        else
        {
            this.#pluginInstance = new SingleOutputPlugin({
                environment: this.#config.environment,
                forceRefresh: this.#config.forceRefresh,
                clientOptions: this.#config.clientOptions,
                indexPrefix: this.#config.indexPrefix ?? 'strata-queue-monitor',
            });
        }
        this.pluginName = this.#pluginInstance.pluginName;
    }

    async init() : Promise<void>
    {
        await this.#pluginInstance.init();
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        await this.#pluginInstance.writeMessage(msg);
    }
}
