import { StrataQueueMonitorMessage } from '../../models/requests.js';

export interface OutputPlugin
{
    pluginName : string;
    init?() : Promise<void>;
    writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>;
}

