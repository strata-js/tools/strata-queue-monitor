import { StrataQueueMonitorMessage } from '../../models/requests.js';
import { OutputPlugin } from '../interfaces/outputPlugin.js';

export interface ConsoleOutputConfig
{
    environment : string;
}

export class ConsoleOutput implements OutputPlugin
{
    #config : ConsoleOutputConfig;
    pluginName = 'Console';

    constructor(config : ConsoleOutputConfig)
    {
        this.#config = config;
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        console.log(`${ this.#config.environment }:${ JSON.stringify(msg) }`);
    }
}
