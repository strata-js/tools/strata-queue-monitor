import { StrataQueueMonitorMessage } from '../../models/requests.js';
import { OutputPlugin } from '../interfaces/outputPlugin.js';

export interface NoneOutputConfig
{
    environment : string;
}

export class NoneOutput implements OutputPlugin
{
    pluginName : string;

    constructor(_config : NoneOutputConfig)
    {
        this.pluginName = 'Do Nothing';
    }

    async writeMessage(_msg : StrataQueueMonitorMessage) : Promise<void>
    {
        // eslint-disable-next-line no-useless-return
        return;
    }
}
