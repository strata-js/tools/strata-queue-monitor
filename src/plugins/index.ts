import { ElasticOutput } from './elastic/index.js';
import { HTTPOutput } from './logstash/index.js';
import { ConsoleOutput, NoneOutput } from './basic/index.js';

export * from './elastic/index.js';

export { OutputPlugin } from './interfaces/outputPlugin.js';

export const plugins = {
    elastic: ElasticOutput,
    logstashHTTP: HTTPOutput,
    console: ConsoleOutput,
    none: NoneOutput,
};
