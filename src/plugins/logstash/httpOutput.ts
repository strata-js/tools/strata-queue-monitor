import { StrataQueueMonitorMessage } from '../../models/requests.js';
import { OutputPlugin } from '../interfaces/outputPlugin.js';
import { NodeHTTPClient } from './httpClient.js';

export interface HTTPOutputConfig
{
    environment : string;
    url : string;
}

export class HTTPOutput implements OutputPlugin
{
    #config : HTTPOutputConfig;
    pluginName = 'Logstash (HTTP)';
    #client ?: NodeHTTPClient;

    constructor(config : HTTPOutputConfig)
    {
        this.#config = config;
        this.#client = new NodeHTTPClient(this.#config.url);
    }

    #getDateStr() : string
    {
        const currDate = new Date();
        const offset = currDate.getTimezoneOffset();
        const newDate = new Date(currDate.getTime() - (offset * 60 * 1000));
        return newDate.toISOString().split('T')[0];
    }

    async writeMessage(msg : StrataQueueMonitorMessage) : Promise<void>
    {
        if(!this.#client)
        {
            throw new Error(`${ this.pluginName } : Tried to call writeMessage() before calling init().`);
        }

        msg['@metadata'] = {
            index: `strata-queue-monitor-${ this.#config.environment }-${ this.#getDateStr() }`,
        };

        // TODO: I need to rework how all plugin errors are handled, but this will do for now.
        try
        {
            await this.#client.makePostRequest(msg);
        }
        catch (err)
        {
            console.error(`Failed to write message ${ JSON.stringify(msg) }`);
            console.error(err);
        }
    }
}
