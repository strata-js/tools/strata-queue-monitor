import http, { RequestOptions } from 'http';
import https from 'https';

// Why not just a library like axios you ask? Well according to this
// https://github.com/nik-kor/http-libraries-perf-test the native node http api is 3x faster.

export class NodeHTTPClient
{
    #protocol : string;
    #urlPath : string;
    #requestOptions : RequestOptions;

    constructor(urlPath : string)
    {
        this.#urlPath = urlPath;
        const parsedURL = new URL(this.#urlPath);
        this.#protocol = parsedURL.protocol;
        this.#requestOptions = this.#buildRequestOptions();
    }

    #buildRequestOptions() : RequestOptions
    {
        return {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        };
    }

    async makePostRequest(postData : Record<string, any>) : Promise<void>
    {
        const httpClient = this.#protocol === 'http:' ? http : https;
        const postDataString = JSON.stringify(postData);
        return new Promise((resolve, reject) =>
        {
            const req = httpClient.request(this.#urlPath, this.#requestOptions);
            req.on('response', (res) =>
            {
                res.on('error', (error) => { reject(error); });
                if(res.statusCode !== 200)
                {
                    reject(res.statusCode);
                }
                resolve();
            });
            req.on('error', (error) => { reject(error); });
            req.write(postDataString);
            req.end();
        });
    }
}
