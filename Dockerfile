#-----------------------------------------------------------------------------------------------------------------------
# Bundle Stage - Do all our bundling of assets
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20 AS bundle-builder

RUN mkdir -p /app
WORKDIR /app

ADD . /app/

RUN npm install
RUN npm run build

#-----------------------------------------------------------------------------------------------------------------------
# Dep Stage - Install production packages and clean cache
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20-alpine AS npm-builder

COPY --from=bundle-builder /app /app

WORKDIR /app

RUN npm install --production --ignore-scripts

#-----------------------------------------------------------------------------------------------------------------------
# Final Docker
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20-alpine

# Only copy the files we actually need
COPY --from=bundle-builder /app/dist /app/dist
COPY --from=npm-builder /app/node_modules /app/node_modules
COPY --from=bundle-builder /app/package.json /app/

RUN mkdir /app/db

WORKDIR /app

VOLUME /app/db

CMD [ "node", "dist/monitor.js", "# sqm server" ]

#-----------------------------------------------------------------------------------------------------------------------

